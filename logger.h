/***************************************************************************
                           logger.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef LOGGER_H
#define LOGGER_H

#include <boost/noncopyable.hpp>
#include <string>
#include "mutex.h"

#define LOG(x,y,...) Logger::getInstance().report(__FILE__,__LINE__,x,y,##__VA_ARGS__);

typedef enum {
  LOG_EMERG   = 0,  /* system is unusable */
  LOG_ALERT   = 1,  /* action must be taken immediately */
  LOG_CRIT    = 2,  /* critical conditions */
  LOG_ERR     = 3,  /* error conditions */
  LOG_WARNING = 4,  /* warning conditions */
  LOG_NOTICE  = 5,  /* normal but significant condition */
  LOG_INFO    = 6,  /* informational */
  LOG_DEBUG   = 7,  /* debug-level messages */
  LOG_LEVEL_MAX = LOG_DEBUG + 1
} LOG_LEVEL;

class Logger :
    public boost::noncopyable
{
private:
  Logger();

public:
  static Logger& getInstance();
  void setLogLevel(LOG_LEVEL level);
  void report(std::string const & module, int32_t line, LOG_LEVEL errlevel, const char *fmt, ... );

private:
  void printMessage(std::string const & module, int32_t line, char *buf);

private:
  LOG_LEVEL m_logLevel;
  Mutex m_lock;
};

#endif // LOGGER_H
