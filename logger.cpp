/***************************************************************************
                           logger.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <sys/time.h>

#include "lock.h"
#include "logger.h"

/***********************************************************************//**
  @method :  Logger
  @comment:  constructor
***************************************************************************/
Logger::Logger() :
  m_logLevel(LOG_NOTICE)
{
}

/***********************************************************************//**
  @method :  getInstance
  @comment:  get singleton instance
***************************************************************************/
Logger& Logger::getInstance()
{
  static Logger m_log;
  return m_log;
}

/***********************************************************************//**
  @method :  report
  @comment:  log with given level
  @param  :  errlevel log level
  @param  :  fmt format
***************************************************************************/
void Logger::report(std::string const & module, int32_t line, LOG_LEVEL errlevel, const char *fmt, ... )
{
  if (errlevel <= m_logLevel) {
    char buf[BUFSIZ];
    va_list ap;

    Lock lock(m_lock);
    strncpy(buf, "VDSM_EXP:", BUFSIZ);
    va_start(ap, fmt);
    vsnprintf(buf + strlen(buf), sizeof(buf)-strlen(buf), fmt, ap);
    va_end(ap);
    printMessage(module, line, buf);
  }
}

/***********************************************************************//**
  @method :  printMessage
  @comment:  print the given message to stderr
  @param  :  buf buffer to print
***************************************************************************/
void Logger::printMessage(std::string const & module, int32_t line, char *buf)
{
  char buf2[BUFSIZ+100], *sp, tsbuf[30];
  struct timeval t;
  gettimeofday(&t, NULL);
  strftime(tsbuf, sizeof(tsbuf), "%Y-%m-%d %H:%M:%S", localtime(&t.tv_sec));
  (void)snprintf(buf2, BUFSIZ, "[%s.%03d]", tsbuf, (int)t.tv_usec / 1000);

//#define DEBUG 1
#ifdef DEBUG
  (void)snprintf(buf2+strlen(buf2), BUFSIZ, "{%s %04d} \t", module.c_str(), line);
#else
  (void) module;
  (void) line;
#endif

  for (sp = buf; *sp != '\0'; sp++) {
    if (isprint(*sp) || (isspace(*sp) && (sp[1]=='\0' || sp[2]=='\0'))) {
      (void)snprintf(buf2+strlen(buf2), 2, "%c", *sp);
    } else {
      (void)snprintf(buf2+strlen(buf2), 6, "\\x%02x", (unsigned)*sp);
    }
  }
  (void)fputs(buf2, stderr);
}

/***********************************************************************//**
  @method :  setLogLevel
  @comment:  set the log level. See also LOG_LEVEL.
  @param  :  level log level to set
***************************************************************************/
void Logger::setLogLevel(LOG_LEVEL level)
{
  m_logLevel = level;
}

