/***************************************************************************
                           ivdcaccess.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IVDCACCESS_H
#define IVDCACCESS_H

#include <stdlib.h>
#include <dsvdc/dsvdc.h>
#include <boost/shared_ptr.hpp>

class IVDC;

class IVDCAccess
{
public:
  IVDCAccess();
  virtual ~IVDCAccess();
  virtual dsvdc_t* getHandle() const = 0;
  virtual boost::shared_ptr<IVDC> getVDC() const = 0;
};

#endif // IVDCACCESS_H
