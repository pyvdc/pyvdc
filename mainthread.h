/***************************************************************************
                           mainthread.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef MAINTHREAD_H
#define MAINTHREAD_H

#include "vdcaccess.h"
#include <pthread.h>

/// forward declaration to setup friend.
void * doRun(void *ptr);

class MainThread
{
public:
  MainThread();
  ~MainThread();
  void startThread();
  void stopThread();
  VDCAccess* getAccess();

  bool setVDCDsuid(char* dsuid);
  bool setVDCName(char* name);

  friend void * doRun(void *ptr);

private:
  void run();

private:
  bool m_running;
  pthread_t m_thread;
  VDCAccess m_access;

  dsuid_t m_vdcDsuid;
  std::string m_vdcName;
};

#endif // MAINTHREAD_H
