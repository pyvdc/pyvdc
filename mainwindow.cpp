/***************************************************************************
                           mainwindow.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  connect (ui->bt_setTemp, SIGNAL(clicked()), this, SLOT(onTempClicked()));
  connect(ui->bt_setButton, SIGNAL(pressed()), this, SLOT(onButtonOn()));
  connect(ui->bt_resetButton, SIGNAL(pressed()), this, SLOT(onButtonOff()));
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::onTempClicked()
{
  QString textField = ui->ed_Temp->toPlainText();
  double data  = textField.toDouble();
  emit sigTempCanged(data);
}

void MainWindow::onButtonOn()
{
  int value = 0;
  emit sigButonEvent(value, true);
}

void MainWindow::onButtonOff()
{
  int value = 0;
  emit sigButonEvent(value, false);
}

