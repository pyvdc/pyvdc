/***************************************************************************
                           vdcaccess.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>

#include <digitalSTROM/dsuid.h>

#include "vdcaccess.h"
#include "lock.h"
#include "logger.h"

namespace // anonymous namespace
{

/***********************************************************************//**
  @method :  hello_cb
  @comment:  callback hello
  @param  :  handle input
  @param  :  dsuid
  @param  :  userdata input
***************************************************************************/
static void hello_cb(dsvdc_t* handle, const char *dsuid,  void* userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  wrapper->hello(handle);
}

/***********************************************************************//**
  @method :  ping_cb
  @comment:  callback ping
  @param  :  handle input
  @param  :  dsuid input
  @param  :  userdata input
***************************************************************************/
static void ping_cb(dsvdc_t* handle , const char* dsuid,  void* userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  wrapper->ping(handle, dsuid);
}

/***********************************************************************//**
  @method :  bye_cb
  @comment:  callback bye
  @param  :  handle input
  @param  :  dsuid input
  @param  :  userdata input
***************************************************************************/
static void bye_cb(dsvdc_t* handle , const char* dsuid,  void* userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  wrapper->bye(handle, dsuid);
}

/***********************************************************************//**
  @method :  getprop_cb
  @comment:  get property callback
  @param  :  handle input
  @param  :  dsuid input
  @param  :  property output
  @param  :  query input
  @param  :  userdata input
***************************************************************************/
static void getprop_cb(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* query,  void* userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  wrapper->getprop(handle, dsuid, property, query);
}

/***********************************************************************//**
  @method :  setprop_cb
  @comment:  set property callback
  @param  :  handle input
  @param  :  dsuid input
  @param  :  property output
  @param  :  properties input
  @param  :  userdata input
***************************************************************************/
static void setprop_cb (dsvdc_t *handle, const char *dsuid, dsvdc_property_t *property, const dsvdc_property_t *properties, void *userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  wrapper->setprop(handle, dsuid, property, properties);
}

/***********************************************************************//**
  @method :  setcontrol_cb
  @comment:  set control value callback
  @param  :  handle input
  @param  :  dsuid inputs (array)
  @param  :  n_dsuid number of dsuid entries in inputs
  @param  :  value value to set
  @param  :  group
  @param  :  zone_id
  @param  :  userdata input
***************************************************************************/
static void setcontrol_cb(dsvdc_t *handle, char **dsuid, size_t n_dsuid,
                 int32_t value, int32_t group, int32_t zone_id,
                 void *userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  LOG(LOG_INFO, "VDCAccess::setControl_cb\n");

  for (size_t n = 0; n < n_dsuid; ++ n) {
    LOG(LOG_INFO, "VDCAccess::setControl_cb:dsuid: %s\n", dsuid[n]);
    wrapper->setcontrol(handle, dsuid[n], value, group, zone_id);
  }

}

/***********************************************************************//**
  @method :  setchannelvalue_cb
  @comment:  set channel value callback
  @param  :  handle input
  @param  :  dsuid inputs (array)
  @param  :  n_dsuid number of dsuid entries in inputs
  @param  :  apply
  @param  :  channel
  @param  :  value
  @param  :  userdata input
***************************************************************************/
static void setchannelvalue_cb(dsvdc_t *handle, char **dsuid, size_t n_dsuid,
                               bool apply, int32_t channel, double value,
                               void *userdata)
{
  VDCAccess* wrapper = reinterpret_cast<VDCAccess*>(userdata);
  LOG(LOG_INFO, "VDCAccess::setchannelvalue_cb\n");

  for (size_t n = 0; n < n_dsuid; ++ n) {
    LOG(LOG_INFO, "VDCAccess::setchannelvalue_cb:dsuid: %s\n", dsuid[n]);
    wrapper->setChannelValue(handle, dsuid[n], apply, value, channel);
  }
}

} // end anonymous namespace

/***********************************************************************//**
  @method :  VDCAccess
  @comment:  constructor
***************************************************************************/
VDCAccess::VDCAccess() :
  m_handle(0),
  m_ready(false)
{
}

/***********************************************************************//**
  @method :  registerVDC
  @comment:  register the vdc (only one item)
  @param  :  vdc input
***************************************************************************/
void VDCAccess::registerVDC(boost::shared_ptr<IVDC> vdc)
{
  m_vdc = vdc;
}

/***********************************************************************//**
  @method :  getVDC
  @comment:  get the registered vdc
  @return :  vdc
***************************************************************************/
boost::shared_ptr<IVDC> VDCAccess::getVDC() const
{
  return m_vdc;
}

/***********************************************************************//**
  @method :  registerVDSD
  @comment:  register a vdsd
  @param  :  vdsd input
***************************************************************************/
void VDCAccess::registerVDSD(boost::shared_ptr<IVDSD> vdsd)
{
  const dsuid_t tempDsuid = vdsd->getDsuid();

  Lock lock(m_AccessMutex);
  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter == m_vdsd.end()) {
    vdsd->registerVdcAcces(this);
    m_vdsd.insert( std::make_pair<dsuid_t, boost::shared_ptr<IVDSD> >( tempDsuid, vdsd));
  }
}

/***********************************************************************//**
  @method :  getHandle
  @comment:  get the handle of the VDCAccess instance
  @return :  handle
***************************************************************************/
dsvdc_t* VDCAccess::getHandle() const
{
  return m_handle;
}

/***********************************************************************//**
  @method :  initializeLib
  @comment:  initialize the library and set the handle
  @return :  true initialized
***************************************************************************/
bool VDCAccess::initializeLib()
{
  if (!m_vdc) {
    return false;
  }

  const dsuid_t tempDsuid = m_vdc->getDsuid();
  char dsuidstring[34];
  ::dsuid_to_string(&tempDsuid, dsuidstring);

  if (dsvdc_new(0, dsuidstring, m_vdc->getName().c_str(), this, &m_handle) != DSVDC_OK) {
    return false;
  }
  return true;
}

/***********************************************************************//**
  @method :  registerCallbacks
  @comment:  register the callbacks in the library
***************************************************************************/
void VDCAccess::registerCallbacks()
{
  /* setup callbacks */
  dsvdc_set_hello_callback(m_handle, hello_cb);
  dsvdc_set_ping_callback(m_handle, ping_cb);
  dsvdc_set_bye_callback(m_handle, bye_cb);
  dsvdc_set_get_property_callback(m_handle, getprop_cb);
  dsvdc_set_set_property_callback(m_handle, setprop_cb);
  dsvdc_set_control_value_callback(m_handle, setcontrol_cb);
  dsvdc_set_output_channel_value_callback(m_handle, setchannelvalue_cb);
}

/***********************************************************************//**
  @method :  unregisterCallbacks
  @comment:  unregister the callbacks in the library,
             which were previously registered.
***************************************************************************/
void VDCAccess::unregisterCallbacks()
{
  dsvdc_set_hello_callback(m_handle, NULL);
  dsvdc_set_ping_callback(m_handle, NULL);
  dsvdc_set_bye_callback(m_handle, NULL);
  dsvdc_set_get_property_callback(m_handle, NULL);
  dsvdc_set_control_value_callback(m_handle, NULL);
  dsvdc_set_output_channel_value_callback(m_handle, NULL);
}

/***********************************************************************//**
  @method :  hello
  @comment:  callback
  @param  :  handle input
***************************************************************************/
void VDCAccess::hello(dsvdc_t */*handle*/)
{
  LOG(LOG_INFO, "VDCAccess::hello\n");
  m_ready = true;
}

/***********************************************************************//**
  @method :  ping
  @comment:  callback
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
***************************************************************************/
void VDCAccess::ping(dsvdc_t* handle , const char* dsuid)
{
  LOG(LOG_NOTICE,"ping %s\n", dsuid);

  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);

  if (m_vdc && m_vdc->isEqual(tempDsuid)) {
    dsvdc_send_pong(handle, dsuid);
    return;
  }

  Lock lock(m_AccessMutex);
  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter!= m_vdsd.end()) {
    dsvdc_send_pong(handle, dsuid);
  }
}

/***********************************************************************//**
  @method :  bye
  @comment:  callback
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
***************************************************************************/
void VDCAccess::bye(dsvdc_t */*handle*/, const char *dsuid)
{
  LOG(LOG_INFO, "VDCAccess::bye %s\n", dsuid);
  m_ready = false;
  resetConnectionState();
}

/***********************************************************************//**
  @method :  getprop
  @comment:  callback
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  property output
  @param  :  query input
***************************************************************************/
void VDCAccess::getprop(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* query)
{
  LOG(LOG_INFO, "VDCAccess::getprop %s\n", dsuid);
  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);

  if (m_vdc && m_vdc->isEqual(tempDsuid)) {
    m_vdc->handleGetVDCProperties(property, query);
  } else {
    Lock lock(m_AccessMutex);
    VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
    if (iter != m_vdsd.end()) {
      boost::shared_ptr<IVDSD> temp = iter->second;
      temp->handleGetVDSDProperties(property, query);
    }
  }

  dsvdc_send_get_property_response(handle, property);
}

/***********************************************************************//**
  @method :  setprop
  @comment:  callback
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  property input
  @param  :  properties input
***************************************************************************/
void VDCAccess::setprop(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* properties)
{
  LOG(LOG_INFO, "VDCAccess::setprop %s\n", dsuid);
  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);

  uint8_t code = DSVDC_ERR_NOT_IMPLEMENTED;

  if (m_vdc && m_vdc->isEqual(tempDsuid)) {
    code = m_vdc->handleSetVDCProperties(property, properties);
  } else {
    Lock lock(m_AccessMutex);
    VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
    if (iter != m_vdsd.end()) {
      boost::shared_ptr<IVDSD> temp = iter->second;
      code = temp->handleSetVDSDProperties(property, properties);
    }
  }
  dsvdc_send_set_property_response(handle, property, code);
}


/***********************************************************************//**
  @method :  setcontrol
  @comment:  set control value to device with group and zone
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  value
  @param  :  group
  @param  :  zone
***************************************************************************/
void VDCAccess::setcontrol(dsvdc_t* /*handle*/, const char* dsuid, int32_t value, int32_t group, int32_t zone)
{
  LOG(LOG_INFO, "VDCAccess::setcontrol %s\n", dsuid);
  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);
  Lock lock(m_AccessMutex);
  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter != m_vdsd.end()) {
    boost::shared_ptr<IVDSD> temp = iter->second;
    temp->handleSetControl(value, group, zone);
  }
}

/***********************************************************************//**
  @method :  setChannelValue
  @comment:  set channel value to device
  @param  :  handle input
  @param  :  dsuid dsuid of vdc or vdcd
  @param  :  apply apply value immeadiately
  @param  :  value
  @param  :  channel
***************************************************************************/
void VDCAccess::setChannelValue(dsvdc_t* /*handle*/, const char* dsuid, bool apply, double value, int32_t channel)
{

  LOG(LOG_INFO, "VDCAccess::setchannelvalue dsuid:%s\n", dsuid);

  dsuid_t tempDsuid;
  dsuid_from_string(dsuid, &tempDsuid);
  Lock lock(m_AccessMutex);

  VDSD_CONST_ITERATOR iter = m_vdsd.find(tempDsuid);
  if (iter != m_vdsd.end()) {
    boost::shared_ptr<IVDSD> temp = iter->second;
    // TODO
    temp->handleSetChannelValue(apply, value, channel);
  }
}


/***********************************************************************//**
  @method :  resetConnectionState
  @comment:  reset the connection state to not connected
***************************************************************************/
void VDCAccess::resetConnectionState()
{
  LOG(LOG_INFO, "VDCAccess::resetConnectionState\n");

  if (m_vdc) {
    m_vdc->resetAnnounced();
  }

  Lock lock(m_AccessMutex);
  for (VDSD_CONST_ITERATOR iter = m_vdsd.begin(); iter!= m_vdsd.end(); ++iter) {
    (iter->second)->sigOffline();
  }
}

/***********************************************************************//**
  @method :  run
  @comment:  this operation must be cyclically called
***************************************************************************/
void VDCAccess::run()
{
  LOG(LOG_DEBUG, "VDCAccess::run\n");
  /* let the work function do our timing, 2secs timeout */

  dsvdc_work(m_handle, 1);
  if (!dsvdc_is_connected(m_handle)) {
    resetConnectionState();
    m_ready = false;
  } else if (m_ready) {
    if (m_vdc) {
      m_vdc->checkAnnounce(m_handle);
    }

    processAnnouncement();
    if (m_vdc) {
      Lock lock(m_AccessMutex);
      for (VDSD_CONST_ITERATOR iter = m_vdsd.begin(); iter != m_vdsd.end(); ++iter) {
        boost::shared_ptr<IVDSD> temp = (iter->second);
        temp->checkAnnounce();
      }
    }
  }
}

/***********************************************************************//**
  @method :  processAnnouncement
  @comment:  process announcement of devices. Only one device announced
***************************************************************************/
void VDCAccess::processAnnouncement()
{
  if (m_vdc) {
    Lock lock(m_AccessMutex);
    // check if any device in state announcing
    bool m_Announcing = false;
    for (VDSD_CONST_ITERATOR iter = m_vdsd.begin(); iter != m_vdsd.end(); ++iter) {
      boost::shared_ptr<IVDSD> temp = (iter->second);
      if (IVDSD::CON_ANNOUNCING == temp->getConnectionState()) {
        m_Announcing = true;
        break;
      }
    }

    // check if device offline
    if (!m_Announcing) {
      for (VDSD_CONST_ITERATOR iter = m_vdsd.begin(); iter != m_vdsd.end(); ++iter) {
        boost::shared_ptr<IVDSD> temp = (iter->second);
        if (IVDSD::CON_OFFLINE == temp->getConnectionState()) {
          temp->sigAnnounce();

          dsuid_t tmpDsuid = temp->getDsuid();
          char dsuidstringVdcd[DSUID_STR_LEN];
          ::dsuid_to_string(&tmpDsuid, dsuidstringVdcd);
          LOG(LOG_INFO, "SEND ANNOUNCE :%s\n", dsuidstringVdcd);
          break;
        }
      }
    }
  }
}


/***********************************************************************//**
  @method :  shutdown
  @comment:  shutdown system. unregister all devices
***************************************************************************/
void VDCAccess::shutdown()
{
  LOG(LOG_DEBUG, "VDCAccess::shutdown\n");
  if (m_vdc) {
    Lock lock(m_AccessMutex);
    for (VDSD_CONST_ITERATOR iter = m_vdsd.begin(); iter != m_vdsd.end(); ++iter) {
      boost::shared_ptr<IVDSD> temp = (iter->second);
      temp->vanish();
      temp.reset();
    }
  }
  dsvdc_cleanup(m_handle);
  m_handle = 0;
}

