/***************************************************************************
                           vdcpylib.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDCPYLIB_H
#define VDCPYLIB_H

#include "vdcpylib_global.h"

#include <QByteArray>

#include <boost/shared_ptr.hpp>

#include "vdsdsimsensor.h"
#include "vdsdsimbutton.h"
#include "vdsdsimoutput.h"

class MainThread;

class VDCPYLib
{
public:
  VDCPYLib();
  ~VDCPYLib();

  void startup();
  void shutdown();

  bool setVdcDsuid(char* dsuid);
  bool setVdcName(char* name);
  void setLogLevel(int level);

  boost::shared_ptr<VDSDSimOutput> createDeviceOutput(char* dsuid);
  boost::shared_ptr<VDSDSimButton> createDeviceButton(char* dsuid);
  boost::shared_ptr<VDSDSimSensor> createDeviceSensor(char* dsuid);

private:
  bool m_started;
  boost::shared_ptr<MainThread> m_thread;
  QByteArray m_buffer;
  std::string m_name;
};

#endif // VDCPYLIB_H
