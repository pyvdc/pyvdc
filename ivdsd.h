/***************************************************************************
                           ivdsd.h
 ***************************************************************************/

/***************************************************************************

 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IVDSD_H
#define IVDSD_H

#include <string>

#include <digitalSTROM/dsuid.h>
#include <dsvdc/dsvdc.h>

// forward declaration
class IVDCAccess;

class IVDSD
{

public:
  typedef enum
  {
    CON_OFFLINE    = 0,
    CON_ANNOUNCING = 1,
    CON_ANNOUNCED  = 2
  } CONNECTION_STATE;

public:
  IVDSD();
  virtual ~IVDSD();

  void setDsuid(const dsuid_t& dsuid);
  dsuid_t getDsuid() const;

  void setIcon(const uint8_t* icon16png, size_t size, std::string &iconName);

  //callback
  void announceDevice(dsvdc_t *handle, int code, void *arg);

  void checkAnnounce();

  void sigOffline();
  void sigAnnounce();

  CONNECTION_STATE getConnectionState() const;

  void registerVdcAcces(IVDCAccess* ivdc);
  IVDCAccess* getVdcAccess() const;

  void handleGetVDSDProperties(dsvdc_property_t *property, const dsvdc_property_t * query);
  uint8_t handleSetVDSDProperties(dsvdc_property_t *property, const dsvdc_property_t * properties);

  virtual void handleSetControl(int32_t value, int32_t group, int32_t zone);
  virtual void handleSetChannelValue(bool apply, double value, int32_t channel);

protected:
  virtual void process() = 0;

public:
  void vanish();

protected:
  virtual void handlePrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleModelFeatures(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleButtonInputDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleButtonInputSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleOutputDescription(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleOutputSettings(dsvdc_property_t *property,const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleChannelDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleChannelSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleBinaryInputDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleBinaryInputSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleSensorDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleSensorSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleSensorStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleBinaryInputStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleName(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleModel(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleConfigURL(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};
  virtual void handleModelUID(dsvdc_property_t *property, const dsvdc_property_t * query, char* name){(void) property, (void)query; (void) name;};


private:
  void handleDeviceIcon(dsvdc_property_t *property, char* name);
  void handleDeviceIconName(dsvdc_property_t *property, char* name);

private:
  dsuid_t m_dsuid;
  uint8_t * m_icon;
  size_t m_iconSize;
  IVDCAccess *m_VDCAccess;
  std::string m_iconName;
  CONNECTION_STATE m_conState;
  bool m_trigAnnounce;
  time_t m_announcingStart;
};

#endif // IVDSD_H
