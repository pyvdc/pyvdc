/***************************************************************************
                           lock.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef CLOCK_H
#define CLOCK_H

#include "mutex.h"

#include <boost/noncopyable.hpp>

/***********************************************************************//**
  @class  : Lock
  @package: Utils
***************************************************************************/
class Lock :
    public boost::noncopyable
{
public:
  explicit Lock(Mutex & mutex);
  ~Lock();

private:
  void lock();
  void release();
  /** local mutex instance */
  Mutex& m_sMutex;
};

#endif
/* CLock_H */
