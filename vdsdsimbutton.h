/***************************************************************************
                           VDSDSimButton.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDSDSIMBUTTON_H
#define VDSDSIMBUTTON_H

#include <boost/noncopyable.hpp>

#include "ivdsd.h"

class VDSDSimButton :
    public IVDSD,
    public boost::noncopyable
{
public:
  VDSDSimButton();

  void setData(int data, bool pressed);
  virtual void process();

protected:
  virtual void handlePrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModelFeatures(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleButtonInputDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleButtonInputSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleConfigURL(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModelUID(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleName(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModel(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);


private:
  time_t m_lastQuery;
};

#endif // VDSDSIMBUTTON_H
