//------------------------------------------------------------------------------
// SWIG Interface
// create with:
// swig -c++ -python VDCPYLIB.i
//
//------------------------------------------------------------------------------

%ignore boost::noncopyable;
namespace boost
{
  class noncopyable {};
}

%module VDCLib

%{
#include "vdcpylib.h"
#include "ivdsd.h"
#include "vdsdsimoutput.h"
#include "vdsdsimsensor.h"
#include "vdsdsimbutton.h"
%}

%include "boost_shared_ptr.i"
%shared_ptr(IVDSD);
%shared_ptr(VDSDSimOutput);
%shared_ptr(VDSDSimSensor);
%shared_ptr(VDSDSimButton);

%include "vdcpylib.h"
%include "ivdsd.h"
%include "vdsdsimoutput.h"
%include "vdsdsimsensor.h"
%include "vdsdsimbutton.h"
