/***************************************************************************
                           mutex.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "mutex.h"

/***********************************************************************//**
  @method :  Mutex
  @comment:  constructor
***************************************************************************/
Mutex::Mutex()
{
  /// initialize lock
  pthread_mutexattr_t     mutexattr;
  int32_t res = ::pthread_mutexattr_init(&mutexattr);
  assert(0==res);

  /// Set the mutex as a recursive mutex
  res = ::pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_RECURSIVE_NP);
  assert(0==res);

  res = ::pthread_mutex_init(&m_tMutex, &mutexattr);
  assert(0==res);

  /// destroy the attribute
  res=::pthread_mutexattr_destroy(&mutexattr);
  assert(0==res);
}

/***********************************************************************//**
  @method :  ~Mutex
  @comment:  destructor
***************************************************************************/
Mutex::~Mutex()
{
  /// Close the mutex
  int32_t res = ::pthread_mutex_destroy (&m_tMutex);
  assert(0==res);
}

/* END_MODULE */
