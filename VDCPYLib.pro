#-------------------------------------------------
#
# Project created by QtCreator 2014-12-04T10:42:33
#
#-------------------------------------------------

QT       -= gui

TARGET = _VDCLib
TEMPLATE = lib

DEFINES += VDCPYLIB_LIBRARY

SOURCES += vdcpylib.cpp \
    logger.cpp \
    lock.cpp \
    ivdsd.cpp \
    ivdcaccess.cpp \
    ivdc.cpp \
    mainthread.cpp \
    mutex.cpp \
    vdc.cpp \
    vdcaccess.cpp \
    vdsdsimsensor.cpp \
    vdsdsimoutput.cpp \
    vdsdsimbutton.cpp \
    VDCPYLIB_wrap.cxx

HEADERS += vdcpylib.h\
        vdcpylib_global.h \
    logger.h \
    lock.h \
    ivdsd.h \
    ivdcaccess.h \
    ivdc.h \
    mainthread.h \
    mutex.h \
    vdc.h \
    vdcaccess.h \
    vdsdsimsensor.h \
    vdsdsimoutput.h \
    vdsdsimbutton.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}


BASEPATH = $$(BASE)

unix {

    INCLUDEPATH += /usr/include/python2.7

    INCLUDEPATH += $${BASEPATH}/DSS/include
    LIBPATH     += $${BASEPATH}/DSS/lib

    LIBS        += -ldsvdc
    LIBS        += -ldsuid

    LIBS        += -lavahi-client

    LIBS        += -luuid
    LIBS        += -lossp-uuid

    LIBPATH     += /usr/local/lib/
    LIBS        += -lprotobuf
    LIBS        += -lprotobuf-c
    LIBS        += -lprotoc
    LIBS        += -lprotobuf-lite

    LIBPATH     += /usr/lib/python2.7/config-x86_64-linux-gnu
    LIBS        += -lpython2.7

}

OTHER_FILES += \
    VDCPYLIB.i

RESOURCES += \
    resource.qrc
