/***************************************************************************
                           vdcpylib.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "vdcpylib.h"
#include "mainthread.h"
#include "logger.h"

#include <QFile>

VDCPYLib::VDCPYLib() :
  m_started(false),
  m_name("VDCTest")
{
  m_thread.reset(new MainThread());

  // read image resource
  QFile file(":/images/vdsd_yellow.png");
  file.open(QIODevice::ReadOnly);
  m_buffer = file.readAll();
}

VDCPYLib::~VDCPYLib()
{
  shutdown();
}

void VDCPYLib::startup()
{
  if (!m_started) {
    m_started = true;
    m_thread->startThread();
  }
}

void VDCPYLib::shutdown()
{
  if (m_started) {
    m_started = false;
    m_thread->stopThread();
  }
}

bool VDCPYLib::setVdcDsuid(char* dsuid)
{
  return m_thread->setVDCDsuid(dsuid);
}

bool VDCPYLib::setVdcName(char* name)
{
  return m_thread->setVDCName(name);
}

void VDCPYLib::setLogLevel(int level)
{
  if  ( (level >= 0) && (level < LOG_LEVEL_MAX)) {
    Logger::getInstance().setLogLevel((LOG_LEVEL) level);
  }
}

boost::shared_ptr<VDSDSimOutput> VDCPYLib::createDeviceOutput(char* dsuid)
{
  dsuid_t vdcd_output;
  dsuid_from_string(dsuid, &vdcd_output);
  VDCAccess* access = m_thread->getAccess();
  boost::shared_ptr<VDSDSimOutput> myOutput;
  myOutput.reset(new VDSDSimOutput());
  myOutput->setDsuid(vdcd_output);
  myOutput->setIcon((const uint8_t*)m_buffer.data(), m_buffer.size(), m_name);
  access->registerVDSD(myOutput);
  return myOutput;
}

boost::shared_ptr<VDSDSimButton> VDCPYLib::createDeviceButton(char* dsuid)
{
  dsuid_t vdcd_button;
  dsuid_from_string(dsuid, &vdcd_button);
  VDCAccess* access = m_thread->getAccess();
  boost::shared_ptr<VDSDSimButton> myButton;
  myButton.reset(new VDSDSimButton());
  myButton->setDsuid(vdcd_button);
  myButton->setIcon((const uint8_t*)m_buffer.data(), m_buffer.size(), m_name);
  access->registerVDSD(myButton);
  return myButton;
}

boost::shared_ptr<VDSDSimSensor> VDCPYLib::createDeviceSensor(char* dsuid)
{
  dsuid_t vdcd_button;
  dsuid_from_string(dsuid, &vdcd_button);
  VDCAccess* access = m_thread->getAccess();
  boost::shared_ptr<VDSDSimSensor> mySensor;
  mySensor.reset(new VDSDSimSensor());
  mySensor->setDsuid(vdcd_button);
  mySensor->setIcon((const uint8_t*)m_buffer.data(), m_buffer.size(), m_name);
  access->registerVDSD(mySensor);
  return mySensor;
}
