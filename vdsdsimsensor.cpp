/***************************************************************************
                           VDSDSimSensor.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "vdsdsimsensor.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ivdcaccess.h"
#include "logger.h"

namespace // anonymous namespace
{
static const int DELAY = 300;
}

/***********************************************************************//**
  @method :  VDSDSimSensor
  @comment:  constructor
***************************************************************************/
VDSDSimSensor::VDSDSimSensor() :
  m_batteryOk(true),
  m_value(0),
  m_lastValue(0)
{
}

/***********************************************************************//**
  @method :  setBatteryState
  @comment:  set the battery state
  @param  :  state true ok, false battery drained
***************************************************************************/
void VDSDSimSensor::setBatteryState(bool state)
{
  m_batteryOk = state;
}

/***********************************************************************//**
  @method :  setData
  @comment:  set temperature value and push it
  @param  :  data temperature value
***************************************************************************/
void VDSDSimSensor::setData(double data)
{
  m_value = data;
  pushData();
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::process()
{
  if (  IVDSD::CON_ANNOUNCED == getConnectionState() ) {
    time_t now = time(NULL);
    if (now-DELAY >= m_lastQuery) {
      pushData();
    }
  }
}

/***********************************************************************//**
  @method :  pushData
  @comment:  push data to vdsm
***************************************************************************/
void VDSDSimSensor::pushData()
{
  dsvdc_property_t* pushEnvelope;
  dsvdc_property_t* propState;
  dsvdc_property_t* prop;

  if (dsvdc_property_new(&prop) != DSVDC_OK) {
    LOG(LOG_WARNING,"process failed to alloceate property \n");
    return;
  }

  time_t now = time(NULL);

  dsvdc_property_new(&pushEnvelope);
  dsvdc_property_new(&propState);

  dsvdc_property_add_double(prop, "value", m_value);
  dsvdc_property_add_int(prop, "age", now - m_lastQuery);
  dsvdc_property_add_int(prop, "error", 0);

  dsvdc_property_add_property(propState, "0", &prop);

  m_lastQuery = now;

  dsvdc_property_add_property(pushEnvelope, "sensorStates", &propState);

  char dsuidstringVdcd[DSUID_STR_LEN];
  dsuid_t temp  = getDsuid();
  ::dsuid_to_string(&temp , dsuidstringVdcd);

  dsvdc_push_property(getVdcAccess()->getHandle(), dsuidstringVdcd, pushEnvelope);
  dsvdc_property_free(pushEnvelope);

}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handlePrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_uint(property, name, 8); // joker device ->
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleBinaryInputDescriptions(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    dsvdc_property_free(reply);
    return;
  }
  dsvdc_property_add_string(nProp, "name", "Battery Status");
  dsvdc_property_add_uint(nProp, "sensorFunction", 9);
  dsvdc_property_add_double(nProp, "updateInterval", 60 * 5);
  dsvdc_property_add_property(reply, "0", &nProp);

  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleBinaryInputSettings(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    free(name);
    return;
  }

  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    dsvdc_property_free(reply);
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_add_uint(nProp, "group", 8);
  dsvdc_property_add_uint(nProp, "sensorFunction", 12);
  dsvdc_property_add_property(reply, "0", &nProp);
  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleSensorDescriptions(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
     LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    free(name);
    return;
  }

  int n = 0;
  int sensorType = 1;
  int sensorUsage = 1; // Indoor
  //char sensorName[64];
  char sensorIndex[64];

  std::string sensorName("VDSDSimSensor-0");

  dsvdc_property_t *nProp;

  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    LOG(LOG_WARNING, "could not allocate property for %s\n", name);
    return;
  }

  dsvdc_property_add_string(nProp, "name", sensorName.c_str());
  dsvdc_property_add_uint(nProp, "sensorType", sensorType);
  dsvdc_property_add_uint(nProp, "sensorUsage", sensorUsage);
  dsvdc_property_add_double(nProp, "aliveSignInterval", 300);

  snprintf(sensorIndex, 64, "%d", n);
  dsvdc_property_add_property(reply, sensorIndex, &nProp);

  char dsuidstringVdcd[DSUID_STR_LEN];
  dsuid_t temp = getDsuid();
  ::dsuid_to_string(&temp, dsuidstringVdcd);

  LOG(LOG_INFO,"dsuid %s sensor %d: %s type %d usage %d\n", dsuidstringVdcd, n, sensorName.c_str(), sensorType, sensorUsage);
  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleSensorSettings(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  char sensorIndex[64];
  int n = 0;
  //for (n = 0; n < dev->mod->values_num; n++) {
  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    dsvdc_property_free(reply);
    return;
  }

  dsvdc_property_add_uint(nProp, "group", 48);
  dsvdc_property_add_uint(nProp, "minPushInterval", 300);
  dsvdc_property_add_double(nProp, "changesOnlyInterval", 300);

  snprintf(sensorIndex, 64, "%d", n);
  dsvdc_property_add_property(reply, sensorIndex, &nProp);

  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleSensorStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  char* sensorIndex;
  dsvdc_property_t *sensorRequest;

  dsvdc_property_get_property_by_index(query, 0, &sensorRequest);
  if (dsvdc_property_get_name(sensorRequest, 0, &sensorIndex) != DSVDC_OK) {
    LOG(LOG_WARNING, "****** could not parse index\n");
  }

  time_t now = time(NULL);

  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_add_double(nProp, "value", m_value);
  dsvdc_property_add_int(nProp, "age", now - m_lastQuery);
  dsvdc_property_add_int(nProp, "error", 0);
  dsvdc_property_add_property(reply, sensorIndex, &nProp);
  m_lastReported = now;

  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleBinaryInputStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    free(name);
    return ;
  }

  int idx;
  char* sensorIndex;
  dsvdc_property_t *sensorRequest;
  dsvdc_property_get_property_by_index(query, 0, &sensorRequest);
  if (dsvdc_property_get_name(sensorRequest, 0, &sensorIndex) != DSVDC_OK) {
    LOG(LOG_WARNING, "****** could not parse index\n");
    idx = -1;
  } else {
    idx = strtol(sensorIndex, NULL, 10);
  }

  dsvdc_property_t *nProp;
  if ((idx == 0) && (dsvdc_property_new(&nProp) == DSVDC_OK)) {
    dsvdc_property_add_bool(nProp, "value", m_batteryOk);
    dsvdc_property_add_int(nProp, "age", 0);
    dsvdc_property_add_int(nProp, "error", 0);
    dsvdc_property_add_property(reply, sensorIndex, &nProp);
  }

  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "0.0.1");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "0.0.0");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleConfigURL(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "https://localhost:10000");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "macAddress:123123"); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "SimSensor:1234"); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleModelUID(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "F271292A3D375F5180EFEAC9FEB3F2F500"); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleName(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string device("Sensor");
  dsvdc_property_add_string(property, name, device.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleModel(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string info("SimSens");
  dsvdc_property_add_string(property, name, info.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimSensor::handleModelGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  char dsuidstringVdcd[DSUID_STR_LEN];
  dsuid_t temp = getDsuid();
  ::dsuid_to_string(&temp, dsuidstringVdcd);

  char info[256];
  strcpy(info, "dsuid:");
  strcat(info, dsuidstringVdcd);
  dsvdc_property_add_string(property, name, info);
}
