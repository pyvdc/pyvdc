/***************************************************************************
                           VDSDSimOutput.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ivdcaccess.h"
#include "logger.h"
#include "vdsdsimoutput.h"

#include <string.h>

/***********************************************************************//**
  @method :  VDSDimOutput
  @comment:  constructor
***************************************************************************/
VDSDSimOutput::VDSDSimOutput() :
  m_apply(false)
{
  for (int i=0; i<MAX_CHANNEL; ++i) {
    m_value[i] = 0.0;
  }
}

/***********************************************************************//**
  @method :  getValue
  @comment:  getter
  @param  :  channel index of array
  @return :  channel value
***************************************************************************/
double VDSDSimOutput::getValue(int channel) const
{
  if ((channel >=0) && (channel < MAX_CHANNEL)) {
    return m_value[channel];
  }
  LOG(LOG_ERR,"getValue:channel out of range:%i\n", channel);
  return -1;
}

/***********************************************************************//**
  @method :  getApply
  @comment:  getter
  @return :  apply value
***************************************************************************/
bool VDSDSimOutput::getApply() const
{
  return m_apply;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::process()
{
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleSetControl(int32_t value, int32_t group, int32_t zone)
{
  // TODO
  LOG(LOG_INFO,"setControl:value:%i, group:%i, zone:%i\n", value, group, zone);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleSetChannelValue(bool apply, double value, int32_t channel)
{
  // TODO
  LOG(LOG_INFO,"handleSetChannelValue:apply: %i, value: %lf, channel: %i \n", apply, value, channel);
  if ((MAX_CHANNEL > channel) && (0 <= channel))  {
    m_value[channel] = value;
  }
  m_apply = apply;
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handlePrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_uint(property, name, 1); // light
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleModelFeatures(dsvdc_property_t *property, const dsvdc_property_t * query, char* name)
{
  (void) property, (void)query; (void) name;

  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_add_bool(reply, "blink" , true);
  dsvdc_property_add_bool(reply, "dontcare" , true);
  dsvdc_property_add_bool(reply, "outmode" , true);
  dsvdc_property_add_bool(reply, "outvalue8" , true);
  dsvdc_property_add_bool(reply, "transt" , true);

  dsvdc_property_add_property(property, name, &reply);
};

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "0.0.1");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "1.0.0");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleConfigURL(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "https://localhost:10000");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "macAddress:1234"); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "SimOutput:1234"); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleModelUID(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "F171292A3D375F5180EFEAC9FEB3F2F500"); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleName(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string device("Output");
  dsvdc_property_add_string(property, name, device.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleModel(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string info("SimOutput");
  dsvdc_property_add_string(property, name, info.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleModelGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  char dsuidstringVdcd[DSUID_STR_LEN];
  dsuid_t temp = getDsuid();
  ::dsuid_to_string(&temp, dsuidstringVdcd);

  char info[256];
  strcpy(info, "dsuid:");
  strcat(info, dsuidstringVdcd);
  dsvdc_property_add_string(property, name, info);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleOutputDescription(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_add_string(reply, "name", "outputSim");
  dsvdc_property_add_uint(reply, "function", 0);          // 0: on off only
  dsvdc_property_add_uint(reply, "outputUsage" , 0);      // 0: undefined
  dsvdc_property_add_bool(reply, "variableRamp" , false); // no ramp
  dsvdc_property_add_string(reply, "type", "output");     // really needed?

  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleOutputSettings(dsvdc_property_t *property,const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_t *nGroup;
  if (dsvdc_property_new(&nGroup) != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    dsvdc_property_free(reply);
    return;
  }

  dsvdc_property_add_uint(reply, "mode", 1); // BINARY
  dsvdc_property_add_bool(reply, "pushChanges", false);
  dsvdc_property_add_property(property, name, &reply);

  // groups
  dsvdc_property_add_bool(nGroup,"0", true); // default
  dsvdc_property_add_bool(nGroup,"1", true); // light
  dsvdc_property_add_property(reply, "groups", &nGroup);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleChannelDescriptions(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    dsvdc_property_free(reply);
    return;
  }
  dsvdc_property_add_uint(nProp, "channelIndex", 0);
  dsvdc_property_add_double(nProp, "max", 100.0);
  dsvdc_property_add_double(nProp, "min", 0.0);
  dsvdc_property_add_string(nProp, "name", "brightness");
  dsvdc_property_add_double(nProp, "resolution" ,1.0);
  dsvdc_property_add_property(reply, "0", &nProp);
  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimOutput::handleChannelSettings(dsvdc_property_t * /*property*/, const dsvdc_property_t * /*query*/, char* /*name*/)
{
  /* nop: no channel settings defined */
}

