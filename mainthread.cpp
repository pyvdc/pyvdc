/***************************************************************************
                           mainthread.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <QFile>
#include <QBuffer>

#include "logger.h"
#include "mainthread.h"

#include "vdc.h"


namespace {
  static char g_default_vdc_dsuid[35] =
  { "84855887E07459ACC0A706EC2CC0FFEE00" };
}

/***********************************************************************//**
  @method :  doRun
  @comment:  a function passed to pthread_create.
             Assumption: the fourth argument of pthread_create is
             the this pointer of the MainThread.
***************************************************************************/
void * doRun(void *ptr)
{
  MainThread* pThread = reinterpret_cast<MainThread*>(ptr);
  pThread->run();
  return 0;
}

/***********************************************************************//**
  @method :  MainThread
  @comment:  constructor
***************************************************************************/
MainThread::MainThread():
  m_running(false),
  m_vdcName("NotSet")
{
  setVDCDsuid(g_default_vdc_dsuid);
}

/***********************************************************************//**
  @method :  ~MainThread
  @comment:  destructor. stop the thread if still running
***************************************************************************/
MainThread::~MainThread()
{
  stopThread();
}

/***********************************************************************//**
  @method :  startThread
  @comment:  start the posix thread.
***************************************************************************/
void MainThread::startThread()
{
  if (!m_running) {
    m_running = (0 == pthread_create( &m_thread, NULL, doRun, this));
  }
}

/***********************************************************************//**
  @method :  stopThread
  @comment:  stop the posix thread.
***************************************************************************/
void MainThread::stopThread()
{
  if (m_running) {
    m_running = false;
    pthread_join(m_thread, NULL);
  }
}

/***********************************************************************//**
  @method :  getAccess
  @comment:  get pointer to VDCACCess
  @return:   pointer to VDCAccess
***************************************************************************/
VDCAccess* MainThread::getAccess()
{
  return &m_access;
}

/***********************************************************************//**
  @method :  setVDCDsuid
  @comment:  set dsuid of vdc. Only allowed when not running
  @param  :  dsuid string representation of dsuid
  @return :  true successfully set
***************************************************************************/
bool MainThread::setVDCDsuid(char* dsuid)
{
  if (!m_running) {
    dsuid_from_string(dsuid, &m_vdcDsuid);
    return true;
  }
  return false;
}

/***********************************************************************//**
  @method :  setVDCName
  @comment:  set name of vdc. Only allowed when not running
  @param  :  name of vdc
  @return :  true sucessfully set
***************************************************************************/
bool MainThread::setVDCName(char* name)
{
  if (!m_running) {
    m_vdcName = name;
    return true;
  }
  return false;
}

/***********************************************************************//**
  @method :  run
  @comment:  The run function of the MainThread.
             It is called from the static function doRun.
***************************************************************************/
void MainThread::run()
{
  Logger::getInstance().setLogLevel(LOG_WARNING);

  // setup vdc
  boost::shared_ptr<VDC> myVdc(new VDC());
  myVdc->setDsuid(m_vdcDsuid);
  myVdc->setName(m_vdcName);

  m_access.registerVDC(myVdc);
  m_access.initializeLib();
  m_access.registerCallbacks();

  while(m_running) {
    m_access.run();
  }

  m_access.unregisterCallbacks();
  m_access.shutdown();
}
