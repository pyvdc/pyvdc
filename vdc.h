/***************************************************************************
                           vdcd.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDC_H
#define VDC_H

#include <boost/noncopyable.hpp>

#include "ivdc.h"

class VDC :
    public IVDC,
    public boost::noncopyable
{
public:
  VDC();
  virtual ~VDC();
  virtual void handleGetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *query);
  virtual uint8_t handleSetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *properties);

};

#endif // VDC_H
