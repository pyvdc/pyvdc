/***************************************************************************
                          lock.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "lock.h"

/***********************************************************************//**
  @method : Lock
  @comment: the constructor locks the given mutex
  @param  : mutex mutex to lock
***************************************************************************/
Lock::Lock(Mutex & mutex) :
  m_sMutex(mutex)
{
  lock();
}

/***********************************************************************//**
  @method :  ~Lock
  @comment:  the destructor unlocks the given mutex
***************************************************************************/
Lock::~Lock()
{
  release();
}

/***********************************************************************//**
  @method :  lock
  @comment:  Lock the mutex
***************************************************************************/
void Lock::lock()
{
  m_sMutex.lock();
}

/***********************************************************************//**
  @method :  release
  @comment:  unlock the mutex
***************************************************************************/
void Lock::release()
{
  m_sMutex.unlock();
}

/* END_MODULE */

