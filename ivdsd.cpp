/***************************************************************************
                           ivdsd.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ivdsd.h"
#include <stdlib.h>
#include <string.h> // memcpy

#include "logger.h"
#include "ivdcaccess.h"
#include "ivdc.h"

namespace //anonymous namespace
{

static const int ANNOUNCE_DELAY = 7;

/***********************************************************************//**
  @method :  announce_device_cb
  @comment:  announce callback.
             This is registered with dsvdc_announce_device.
             It is called, when the device was accepted after
             registering it with dsvdc_announce_device
  @param  :  handle
  @param  :  code
  @param  :  arg the this pointer (must be correctly registered with dsvdc_announce_device)
  @param  :  userdata ?
***************************************************************************/
void announce_device_cb(dsvdc_t *handle, int code, void *arg, void *userdata)
{
  LOG(LOG_INFO, "IVDSD::device ANNOUNCED Callback\n");
  IVDSD* ivdsd = reinterpret_cast<IVDSD*>(arg);
  ivdsd->announceDevice(handle, code, userdata);
}

}; //anonymous namespace

/***********************************************************************//**
  @method :  IVDSD
  @comment:  contructor
***************************************************************************/
IVDSD::IVDSD() :
  m_icon(NULL),
  m_iconSize(0),
  m_VDCAccess(NULL),
  m_conState(CON_OFFLINE),
  m_trigAnnounce(false)
{
}

/***********************************************************************//**
  @method :  ~IVDSD
  @comment:  destructor
***************************************************************************/
IVDSD::~IVDSD()
{
  free (m_icon);
}

/***********************************************************************//**
  @method :  setDsuid
  @comment:  set dsuid of device
  @param  :  dsuid input
***************************************************************************/
void IVDSD::setDsuid(const dsuid_t& dsuid)
{
  m_dsuid = dsuid;
}

/***********************************************************************//**
  @method :  getDsuid
  @comment:  get dsuid of device
  @return :  dsuid
***************************************************************************/
dsuid_t IVDSD::getDsuid() const
{
  return m_dsuid;
}

/***********************************************************************//**
  @method :  setIcon
  @comment:  set Icon of device.
             image must be of type png 16
  @param  :  icon16png icon
  @param  :  size size of the icon
  @param  :  iconName name
***************************************************************************/
void IVDSD::setIcon(const uint8_t* icon16png, size_t size, std::string &iconName)
{
  if (m_icon) {
    free (m_icon);
    m_icon = NULL;
  }

  m_icon = (uint8_t*)malloc(size);
  memcpy(m_icon, icon16png, size);
  m_iconSize = size;
  m_iconName = iconName;
}

/***********************************************************************//**
  @method :  checkAnnounce
  @comment:  check announce state of device.
             called cyclically.
***************************************************************************/
void IVDSD::checkAnnounce()
{
  assert(m_VDCAccess);

  char dsuidstringVdcd[DSUID_STR_LEN];
  ::dsuid_to_string(&m_dsuid, dsuidstringVdcd);

  if (m_conState == CON_OFFLINE) {

    if (m_trigAnnounce) {
      m_trigAnnounce = false;

      char dsuidstringContainer[DSUID_STR_LEN];
      dsuid_t vdc_dsuid = m_VDCAccess->getVDC()->getDsuid();
      ::dsuid_to_string(&vdc_dsuid, dsuidstringContainer);

      if (dsvdc_announce_device(m_VDCAccess->getHandle(), dsuidstringContainer, dsuidstringVdcd, this, announce_device_cb) == DSVDC_OK) {
        LOG(LOG_DEBUG, "IVDSD::device OFFLINE: %s\n", dsuidstringVdcd);
        m_conState = CON_ANNOUNCING;
      }
    }
  }else if (m_conState == CON_ANNOUNCING) {
    LOG(LOG_DEBUG, "IVDSD::device ANNOUNCING:%s \n", dsuidstringVdcd);

    time_t now = time(NULL);
    if ((now-ANNOUNCE_DELAY) > m_announcingStart) {
      LOG(LOG_WARNING, "IDVDSD:: device ANNOUNCING timeout -> falling back to OFFLINE: %s\n", dsuidstringVdcd);
      m_conState = CON_OFFLINE;
    }


  } else {
    char dsuidstringVdcd[DSUID_STR_LEN];
    ::dsuid_to_string(&m_dsuid, dsuidstringVdcd);
    LOG(LOG_DEBUG, "IVDSD::device ANNOUNCED: %s\n", dsuidstringVdcd);
    process();
  }
}

/***********************************************************************//**
  @method :  announceDevice
  @comment:  callback after registration
  @param  :  handle
  @param  :  code
  @param  :  arg
***************************************************************************/

void IVDSD::announceDevice(dsvdc_t * /*handle*/, int code, void */*userdata*/)
{
  if ((CON_ANNOUNCING == m_conState) && (0 == code)) {
    m_conState = CON_ANNOUNCED;
    LOG(LOG_INFO, "IVDSD::device ANNOUNCED CB code:%i\n", code);
  } else {
    LOG(LOG_WARNING, "IVDSD::device ANNOUNCED CB  failed code:%i\n", code);
  }
}

/***********************************************************************//**
  @method :  sigOffline
  @comment:  reset announced state to offline
***************************************************************************/
void IVDSD::sigOffline()
{
  LOG(LOG_DEBUG, "IVDSD::sigOffline\n");
  m_conState = CON_OFFLINE;
}

/***********************************************************************//**
  @method :  sigAnnounce
  @comment:  start announce operation
***************************************************************************/
void IVDSD::sigAnnounce()
{
  LOG(LOG_DEBUG, "IVDSD::sigAnnounce\n");
  m_trigAnnounce = true;
  m_announcingStart = time(NULL);
}

/***********************************************************************//**
  @method :  getConnectionState
  @comment:  get the connection state
  @param  :  state
***************************************************************************/
IVDSD::CONNECTION_STATE IVDSD::getConnectionState() const
{
  return m_conState;
}

/***********************************************************************//**
  @method :  registerVdcAccess
  @comment:  register VDC container
  @param  :  ivdc input
***************************************************************************/
void IVDSD::registerVdcAcces(IVDCAccess* ivdc)
{
  m_VDCAccess = ivdc;
}

/***********************************************************************//**
  @method :  getVdcAccess
  @comment:  get the vdc container
  @return :  container
***************************************************************************/
IVDCAccess* IVDSD::getVdcAccess() const
{
  assert(m_VDCAccess);
  return m_VDCAccess;
}

/***********************************************************************//**
  @method :  handleGetVDSDProperties
  @comment:  handle a property query.
             Overwrite the handle... in your own class, if needed.
  @param  :  handle handle of container
  @param  :  property property to fill data
  @param  :  query property query
***************************************************************************/
void IVDSD::handleGetVDSDProperties(dsvdc_property_t *property, const dsvdc_property_t * query)
{
  char *name;

  for (size_t i = 0; i < dsvdc_property_get_num_properties(query); i++) {
    int ret = dsvdc_property_get_name(query, i, &name);
    if (ret != DSVDC_OK) {
      return;
    }

    if (!name)
    {
      LOG(LOG_DEBUG, "IVDSD:: handleVDSDProperties not yet handling wildcard properties\n");
      continue;
    }

    LOG(LOG_DEBUG, "**** request name: %s\n", name);
    if (0 == strcmp(name, "primaryGroup"))
    {
      handlePrimaryGroup(property, query, name);
    }
    else if (0 == strcmp(name, "modelFeatures"))
    {
      handleModelFeatures(property, query, name);
    }
    else if (0 == strcmp(name, "buttonInputDescriptions"))
    {
      handleButtonInputDescriptions(property, query, name);
    }
    else if (0 == strcmp(name, "buttonInputSettings"))
    {
      handleButtonInputSettings(property, query, name);
    }
    else if (0 == strcmp(name, "outputDescription"))
    {
      handleOutputDescription(property, query, name);
    }
    else if (0 == strcmp(name, "outputSettings"))
    {
      handleOutputSettings(property, query, name);
    }
    else if (0 == strcmp(name, "channelDescriptions"))
    {
      handleChannelDescriptions(property, query, name);
    }
    else if (0 == strcmp(name, "channelSettings"))
    {
      handleChannelSettings(property, query, name);
    }
    else if (0 == strcmp(name, "binaryInputDescriptions"))
    {
      handleBinaryInputDescriptions(property, query, name);
    }
    else if (0 == strcmp(name, "binaryInputSettings"))
    {
      handleBinaryInputSettings(property, query, name);
    }
    else if (0 == strcmp(name, "sensorDescriptions"))
    {
      handleSensorDescriptions(property, query, name);
    }
    else if (0 == strcmp(name, "sensorSettings"))
    {
      handleSensorSettings(property, query, name);
    }
    else if (0 == strcmp(name, "sensorStates"))
    {
      handleSensorStates(property,query, name);
    }
    else if (0 == strcmp(name, "binaryInputStates"))
    {
      handleBinaryInputStates(property, query, name);
    }
    else if (0 == strcmp(name, "name"))
    {
      handleName(property, query, name);
    }
    else if (0 == strcmp(name, "model"))
    {
      handleModel(property, query, name);
    }
    else if (0 == strcmp(name, "modelGuid"))
    {
      handleModelGuid(property, query, name);
    }
    else if (0 == strcmp(name, "vendorGuid"))
    {
      handleVersionGuid(property, query, name);
    }
    else if (0 == strcmp(name, "hardwareVersion"))
    {
      handleHardwareVersion(property, query, name);
    }
    else if (0 == strcmp(name, "configURL"))
    {
      handleConfigURL(property, query, name);
    }
    else if (0 == strcmp(name, "hardwareGuid"))
    {
      handleHardwareGuid(property, query, name);
    }
    else if (0 == strcmp(name, "hardwareModelGuid"))
    {
      handleHardwareModelGuid(property, query, name);
    }
    else if (0 == strcmp(name, "modelUID"))
    {
      handleModelUID(property, query, name);
    }
    else if (0 == strcmp(name, "deviceIcon16"))
    {
      handleDeviceIcon(property, name);
    }
    else if (0 == strcmp(name, "deviceIconName"))
    {
      handleDeviceIconName(property, name);
    }
    else
    {
      LOG(LOG_WARNING, "IVDSD handleVDSDProperties property unknown: name: %s\n", name);
    }
  }
  free(name);
}

/***********************************************************************//**
  @method :  handleSetVDSDProperties
  @comment:  handle a set property query.
             Overwrite the handle... in your own class, if needed.
  @param  :  handle handle of container
  @param  :  property property to fill data
  @param  :  properties property query
***************************************************************************/
uint8_t IVDSD::handleSetVDSDProperties(dsvdc_property_t *property, const dsvdc_property_t * properties)
{
  // TODO
  return DSVDC_ERR_NOT_IMPLEMENTED;
}

/***********************************************************************//**
  @method :  handleSetControl
  @comment:  handle the control callback
  @param  :  value ..
  @param  :  group ..
  @param  :  zone ..
***************************************************************************/
void IVDSD::handleSetControl(int32_t value, int32_t group, int32_t zone)
{
  /* nop */
  LOG(LOG_DEBUG, "IVDSD handleSetControl not implemented value: %i, group:%i, zone :%i\n", value, group, zone);
}

/***********************************************************************//**
  @method :  handleSetControl
  @comment:  handle the control callback
  @param  :  value ..
  @param  :  group ..
  @param  :  zone ..
***************************************************************************/
void IVDSD::handleSetChannelValue(bool apply, double value, int32_t channel)
{
  /* todo*/
  LOG(LOG_DEBUG, "IVDSD handleSetChannelValue not implemented apply: %i, value: %d, gchannel:%i\n", apply, value, channel);
}

/***********************************************************************//**
  @method :  vanish
  @comment:  send vanish device and set state to offline
***************************************************************************/
void IVDSD::vanish()
{
  LOG(LOG_DEBUG, "IVDSD::vanish\n");
  assert(m_VDCAccess);

  if (m_conState != CON_OFFLINE) {
    char dsuidstringVdcd[DSUID_STR_LEN];
    ::dsuid_to_string(&m_dsuid, dsuidstringVdcd);
    dsvdc_device_vanished(m_VDCAccess->getHandle(), dsuidstringVdcd);
  }

  sigOffline();
}

/***********************************************************************//**
  @method :  handleDeviceIcon
  @comment:  handler for device icon
  @param  :  property fill data
  @param  :  name name of property
***************************************************************************/
void IVDSD::handleDeviceIcon(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceIcon\n");
  dsvdc_property_add_bytes(property, name, m_icon, m_iconSize);
}

/***********************************************************************//**
  @method :  handleDeviceIconName
  @comment:  handler for device icon name
  @param  :  property fill data
  @param  :  name name of property
***************************************************************************/
void IVDSD::handleDeviceIconName(dsvdc_property_t *property, char* name)
{
  LOG(LOG_DEBUG, "IVDSD::handleDeviceIconName\n");
  dsvdc_property_add_string(property, name, m_iconName.c_str());
}

