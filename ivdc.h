/***************************************************************************
                           ivdc.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef IVDSM_H
#define IVDSM_H

#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>

#include <string>

#include <digitalSTROM/dsuid.h>
#include <dsvdc/dsvdc.h>

class IVDC :
    public boost::enable_shared_from_this<IVDC>
{
public:
  typedef enum
  {
    CON_OFFLINE    = 0,
    CON_ANNOUNCING = 1,
    CON_ANNOUNCED  = 2
  } CONNECTION_STATE;

  IVDC();
  virtual ~IVDC();

  boost::shared_ptr<IVDC> getptr()
  {
    return shared_from_this();
  };

  void setDsuid(const dsuid_t& dsuid);
  dsuid_t getDsuid() const;

  bool isEqual(const dsuid_t& dsuid) const;

  void setName(std::string &name);
  std::string getName() const;

  void announceContainer(dsvdc_t *handle, int code, void *arg);
  void checkAnnounce(dsvdc_t *handle);
  void resetAnnounced();

  virtual void handleGetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *query) = 0;
  virtual uint8_t handleSetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *properties) = 0;

private:
  dsuid_t m_dsuid;
  std::string m_name;
  CONNECTION_STATE m_conState;
};

#endif // IVDSM_H
