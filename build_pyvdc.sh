#!/bin/bash

#------------------------------------------------------------------------------
# @comment: this script is for ubuntu > 13.xx. It sets up all needed packages
#           and installs all needed libraries to use vdcpylib.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# @comment: checks if a package is installed. If not -> install it.
# @param1: list of packages to install
#------------------------------------------------------------------------------
function checkInstall {
  for pkg in $*;
  do
    PKG_OK=$(dpkg-query -W --showformat='${Status}\n' ${pkg}| grep "install ok installed")
    echo Checking for ${pkg}: $PKG_OK
    if [ "" == "$PKG_OK" ]; then
      echo "No somelib. Setting up somelib."
      sudo apt-get --force-yes --yes install ${pkg}
    fi
  done;
}

											
#------------------------------------------------------------------------------
# main

# install ubuntu packages
checkInstall avahi-daemon libavahi-client-dev uuid-dev libossp-uuid-dev python python2.7 libpython2.7-dev protobuf-compiler protobuf-c-compiler libprotoc-dev qt5-default automake cmake libtool uthash-dev swig liblua5.1-0 libboost-dev

#------------------------------------------------------------------------------
# get ds packages

DIR=$PWD
export BASE=$DIR
export PKG_CONFIG_PATH=$DIR/DSS/lib/pkgconfig

git clone https://git.digitalstrom.org/ds485-stack/libdsuid.git
git clone https://git.digitalstrom.org/ds485-stack/ds485-core.git
git clone https://git.digitalstrom.org/ds485-stack/ds485-netlib.git
git clone https://git.digitalstrom.org/ds485-stack/ds485-client.git
git clone https://git.digitalstrom.org/ds485-stack/dsm-api.git
git clone https://git.digitalstrom.org/virtual-devices/libdsvdc.git
git clone https://git.digitalstrom.org/ds485-stack/libdsuid.git

echo "--- DS485-core ----------------------------------------------------------"
cd ds485-core/
aclocal
autoreconf -i
./configure --prefix=$DIR/DSS --with-dependency-search=$DIR/DSS 
make
make install
cd $DIR

echo "--- libdsuid ------------------------------------------------------------"
cd libdsuid
aclocal
autoreconf -i
./configure --prefix=$DIR/DSS --with-dependency-search=$DIR/DSS 
make
make install
cd $DIR

echo "--- DS485-netlib --------------------------------------------------------"
cd ds485-netlib
aclocal
autoreconf -i
./configure --prefix=$DIR/DSS --with-dependency-search=$DIR/DSS
make
make install
cd $DIR

echo "--- DS485-client --------------------------------------------------------"
cd ds485-client
aclocal
autoreconf -i
./configure --prefix=$DIR/DSS --with-dependency-search=$DIR/DSS
make
make install
cd $DIR

echo "--- dsm-api -------------------------------------------------------------"
cd dsm-api
cmake -DWITH_SCRIPTING=ON -DCMAKE_INSTALL_PREFIX=$DIR/DSS -DDS485-CLIENT_LIBRARIES=$DIR/DSS/lib -DDS485-CLIENT_INCLUDE_DIRS=$DIR/DSS/include
make 
make install
cd $DIR




echo "--- libdsvdc ------------------------------------------------------------"
cd libdsvdc
aclocal
autoreconf -i
./configure --prefix=$DIR/DSS --with-dependency-search=$DIR/DSS
make
make install
cd $DIR
	
#------------------------------------------------------------------------------
# get pyvdc
git clone https://gitlab.com/pyvdc/pyvdc.git
cd pyvdc
qmake
make
cp lib_VDCLib.so _VDCLib.so
cp startupPyVDC.sh  $DIR
cd $DIR

echo "start with pyvdc:  source startupPyVDC.sh"







