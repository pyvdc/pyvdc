/***************************************************************************
                           VDSDSimSensor.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDSDSimSensor_H
#define VDSDSimSensor_H

#include <boost/noncopyable.hpp>

#include "ivdsd.h"

class VDSDSimSensor :
    public IVDSD,
    public boost::noncopyable
{
public:
  VDSDSimSensor();

  void setBatteryState(bool state);
  void setData(double data);

  virtual void process();

protected:
  virtual void handlePrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleBinaryInputDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleBinaryInputSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleSensorDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleSensorSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleSensorStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleBinaryInputStates(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleConfigURL(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModelUID(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleName(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModel(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);

private:
  void pushData();

private:
  bool m_batteryOk;

  double m_value;
  double m_lastValue;
  time_t m_lastQuery;
  time_t m_lastReported;
};

#endif // VDC_H
