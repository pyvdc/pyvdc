#!/bin/bash

# call this in base directory. source startupPyVDC.sh

DIR=$PWD

export LD_LIBRARY_PATH=$DIR/DSS/lib:$LD_LIBRARY_PATH
export PYTHONPATH=$DIR/DSS:$DIR/DSS/lib:$DIR/DSS/lib/python2.7:$DIR/DSS/lib/python2.7/dist-packages:$DIR/DSS/lib/python2.7/dist-packages/dsmapi2:$PYTHONPATH
export DSDATA_PATH=$DIR/DSS/usr/share/digitalstrom

# call ipython
#

