/***************************************************************************
                           vdc.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logger.h"
#include "vdc.h"

/***********************************************************************//**
  @method :  VDC
  @comment:  constructor
***************************************************************************/
VDC::VDC()
{
}

/***********************************************************************//**
  @method :  ~VDC
  @comment:  destructor
***************************************************************************/
VDC::~VDC()
{
}

/***********************************************************************//**
  @method :  handleVDCProperties
  @comment:  TODO modify for your own needs.
             Example code only
***************************************************************************/
void VDC::handleGetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *query)
{
  char *name;
  for (unsigned int i = 0; i < dsvdc_property_get_num_properties(query); ++i) {

    int ret = dsvdc_property_get_name(query, i, &name);
    if (ret != DSVDC_OK) {
      LOG(LOG_WARNING, "handleVDCProperties: error getting property name, abort\n");
      return;
    }
    if (!name) {
      LOG(LOG_WARNING, "handleVDCProperties: not yet handling wildcard properties\n");
      return;
    }
    LOG(LOG_INFO, "**** request name: %s\n", name);

    if (strcmp(name, "hardwareGuid") == 0) {
      char info[256];
      char buffer[32];

      // TODO
      strcpy(info, "macaddress:");
      sprintf(buffer, "%02x:%02x:%02x:%02x:%02x:%02x",1,2,3,4,5,6); // TODO

      strcat(info, buffer);
      dsvdc_property_add_string(property, name, info);

    } else if (strcmp(name, "modelGuid") == 0) {
      dsvdc_property_add_string(property, name, "HeatingTest VDC Prototype by mry [modelGuid]");

    } else if (strcmp(name, "vendorId") == 0) {
      dsvdc_property_add_string(property, name, "test@test.com");

    } else if (strcmp(name, "name") == 0) {
      dsvdc_property_add_string(property, name, "HeatingTest VDC");

    } else if (strcmp(name, "model") == 0) {
      dsvdc_property_add_string(property, name, "HeatingTest VDC Prototype by mry [model]");

    } else if (strcmp(name, "capabilities") == 0) {
      dsvdc_property_t *reply;
      ret = dsvdc_property_new(&reply);
      if (ret != DSVDC_OK) {
        LOG(LOG_INFO, "failed to allocate reply property for %s\n", name);
        free(name);
        continue;
      }
      dsvdc_property_add_bool(reply, "metering", false);

    } else if (strcmp(name, "configURL") == 0) {
      dsvdc_property_add_string(property, name, "https://localhost:8080");
    }

    free(name);
  }

  return;
}

/***********************************************************************//**
  @method :  handleSetVDCProperties
  @comment:  TODO modify for your own needs.
             Example code only
***************************************************************************/

uint8_t VDC::handleSetVDCProperties(dsvdc_property_t *property, const dsvdc_property_t *properties)
{
  // TODO
  return DSVDC_ERR_NOT_IMPLEMENTED;
}
