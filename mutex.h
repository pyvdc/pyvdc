/***************************************************************************
                           mutex.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef Mutex_H
#define Mutex_H

#include <cassert>
#include <pthread.h>
#include <stdint.h>

#include <boost/noncopyable.hpp>

/***********************************************************************//**
  @class  : Mutex
  @package: Utils
***************************************************************************/
class Mutex :
    public boost::noncopyable
{
public:
  explicit Mutex();
  ~Mutex();

private:
  // mutex internal use
  pthread_mutex_t m_tMutex;

public:

  /***************************************************************************
  @method :  Lock
  @comment:  lock the mutex
***************************************************************************/
  inline void lock()
  {
    int32_t retVal = ::pthread_mutex_lock (&m_tMutex);
    assert(0 == retVal);
  };

  /***************************************************************************
  @method :  Unlock
  @comment:  unlock the mutex
***************************************************************************/
  inline void unlock()
  {
    int32_t retVal = ::pthread_mutex_unlock (&m_tMutex);
    assert(0 == retVal);
  };

};

#endif
/* Mutex_H */
