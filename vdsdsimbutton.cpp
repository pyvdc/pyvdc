/***************************************************************************
                           VDSDSimButton.cpp
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "ivdcaccess.h"
#include "logger.h"
#include "vdsdsimbutton.h"

#include <string.h>


/***********************************************************************//**
  @method :  VDSDSimButton
  @comment:  constructor
***************************************************************************/
VDSDSimButton::VDSDSimButton()
{
  m_lastQuery = time(NULL);
}

/***********************************************************************//**
  @method :  setData
  @comment:  push data to vdsm
  @param  :  data klick type
              0:! tip_1x
              1:! tip_2x
              2:! tip_3x
              3:! tip_4x
              4:! hold_start
              5:! hold_repeat
              6:! hold_end
              7:! click_1x
              8:! click_2x
              9:! click_3x
              10:! short_long
              11:! local_off
              12:! local_on
              13:! short_short_long
              14:! local_stop
              255:! idle (no recent click)
  @param  :  pressed: true button is pressed
***************************************************************************/
void VDSDSimButton::setData(int data, bool pressed)
{
  dsvdc_property_t* pushEnvelope;
  dsvdc_property_t* propState;
  dsvdc_property_t* prop;

  if (dsvdc_property_new(&prop) != DSVDC_OK) {
    LOG(LOG_WARNING,"process failed to alloceate property \n");
    return;
  }

  time_t now = time(NULL);

  dsvdc_property_new(&pushEnvelope);
  dsvdc_property_new(&propState);

  dsvdc_property_add_double(prop, "age", now - m_lastQuery);
  dsvdc_property_add_uint(prop, "clickType", data);
  dsvdc_property_add_uint(prop, "error", 0);
  dsvdc_property_add_bool(prop, "value", pressed);

  dsvdc_property_add_property(propState, "0", &prop);

  m_lastQuery = now;

  dsvdc_property_add_property(pushEnvelope, "buttonInputStates", &propState);

  char dsuidstringVdcd[DSUID_STR_LEN];
  dsuid_t temp  = getDsuid();
  ::dsuid_to_string(&temp , dsuidstringVdcd);

  dsvdc_push_property(getVdcAccess()->getHandle(), dsuidstringVdcd, pushEnvelope);
  dsvdc_property_free(pushEnvelope);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::process()
{
  /* nop */
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handlePrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_uint(property, name, 1);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleModelFeatures(dsvdc_property_t *property, const dsvdc_property_t * query, char* name)
{
  (void) property, (void)query; (void) name;

  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_add_bool(reply, "highlevel" , true);
  dsvdc_property_add_bool(reply, "jokerconfig" , true);
  dsvdc_property_add_bool(reply, "pushbadvanced" , true);
  dsvdc_property_add_bool(reply, "pushbarea" , true);
  dsvdc_property_add_bool(reply, "pushbcombined" , true);
  dsvdc_property_add_bool(reply, "pushbsensor" , true);
  dsvdc_property_add_bool(reply, "pushbutton" , true);
  dsvdc_property_add_bool(reply, "twowayconfig" , true);

  dsvdc_property_add_property(property, name, &reply);
};


/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleButtonInputDescriptions(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    dsvdc_property_free(reply);
    return;
  }

  dsvdc_property_add_uint(  nProp, "buttonElementID", 0);// Element of multi contact btn.
  dsvdc_property_add_uint(  nProp, "buttonID", 0);       // ID of physical button
  dsvdc_property_add_uint(  nProp, "buttonType", 1);     // single push button
  dsvdc_property_add_string(nProp, "name", "SimButton");
  dsvdc_property_add_bool(  nProp, "supportsLocalKeyMode", false);
  dsvdc_property_add_string(nProp, "type", "buttonInput");

  dsvdc_property_add_property(reply, "0", &nProp);
  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleButtonInputSettings(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_t *reply;
  int ret = dsvdc_property_new(&reply);
  if (ret != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    return;
  }

  dsvdc_property_t *nProp;
  if (dsvdc_property_new(&nProp) != DSVDC_OK) {
    LOG(LOG_WARNING, "failed to allocate reply property for %s\n", name);
    dsvdc_property_free(reply);
    return;
  }

  dsvdc_property_add_bool(nProp, "callsPresent", false);
  dsvdc_property_add_uint(nProp, "channel", 0);
  dsvdc_property_add_uint(nProp, "function", 5);
  dsvdc_property_add_uint(nProp, "group", 1);
  dsvdc_property_add_uint(nProp, "mode", 0); // standard
  dsvdc_property_add_bool(nProp, "setsLocalPriority", false);
  dsvdc_property_add_property(reply, "0", &nProp);

  dsvdc_property_add_property(property, name, &reply);
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "0.0.1");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "0.0.0");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleConfigURL(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "https://localhost:10000");
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "macAddress:242424"); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "SimButton:1234"); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleModelUID(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  dsvdc_property_add_string(property, name, "F071292A3D375F5180EFEAC9FEB3F2F500"); // TODO
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleName(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string device("Button");
  dsvdc_property_add_string(property, name, device.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleModel(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  std::string info("SimBtn");
  dsvdc_property_add_string(property, name, info.c_str());
}

/***********************************************************************//**
 * {@inheritdoc}
***************************************************************************/
void VDSDSimButton::handleModelGuid(dsvdc_property_t *property, const dsvdc_property_t * /*query*/, char* name)
{
  char dsuidstringVdcd[DSUID_STR_LEN];
  dsuid_t temp = getDsuid();
  ::dsuid_to_string(&temp, dsuidstringVdcd);

  char info[256];
  strcpy(info, "dsuid:");
  strcat(info, dsuidstringVdcd);
  dsvdc_property_add_string(property, name, info);
}

