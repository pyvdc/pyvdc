/***************************************************************************
                           VDSDSimOutput.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDSDSIMOUTPUT_H
#define VDSDSIMOUTPUT_H

#include <boost/noncopyable.hpp>

#include "ivdsd.h"

class VDSDSimOutput :
    public IVDSD,
    public boost::noncopyable
{
public:
  VDSDSimOutput();

  double getValue(int channel) const;
  bool getApply() const;

  virtual void process();
  virtual void handleSetControl(int32_t value, int32_t group, int32_t zone);
  virtual void handleSetChannelValue(bool apply, double value, int32_t channel);

protected:
  virtual void handlePrimaryGroup(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModelFeatures(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleVersionGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleHardwareVersion(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleConfigURL(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleHardwareGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleHardwareModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModelUID(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleName(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModel(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleModelGuid(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);

  virtual void handleOutputDescription(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleOutputSettings(dsvdc_property_t *property,const dsvdc_property_t * query, char* name);
  virtual void handleChannelDescriptions(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);
  virtual void handleChannelSettings(dsvdc_property_t *property, const dsvdc_property_t * query, char* name);

private:

  enum {
    MAX_CHANNEL = 10
  };

  bool m_apply;
  double m_value[MAX_CHANNEL];

};

#endif // VDSDSIMOUTPUT_H
