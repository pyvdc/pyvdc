/***************************************************************************
                           vdcaccess.h
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef VDCAccess_H
#define VDCAccess_H

#include <stdlib.h>
#include <map>
#include <string>

#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>

#include <digitalSTROM/dsuid.h>
#include <dsvdc/dsvdc.h>

#include "ivdcaccess.h"
#include "ivdc.h"
#include "ivdsd.h"
#include "mutex.h"

namespace std
{
    template<> struct less<dsuid_t>
    {
       bool operator() (const dsuid_t& lhs, const dsuid_t& rhs)
       {
         char temp1[DSUID_STR_LEN];
         ::dsuid_to_string(&lhs, temp1);

         char temp2[DSUID_STR_LEN];
         ::dsuid_to_string(&rhs, temp2);

         return (strcmp(temp1, temp2) > 0);
       }
    };
};

class VDCAccess :
    public IVDCAccess,
    public boost::noncopyable
{
public:
  VDCAccess();
  bool initializeLib();
  void registerCallbacks();
  void unregisterCallbacks();

  void hello(dsvdc_t* handle);
  void ping(dsvdc_t* handle , const char* dsuid);
  void bye(dsvdc_t* handle , const char* dsuid);
  void getprop(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* query);
  void setprop(dsvdc_t* handle, const char* dsuid, dsvdc_property_t* property, const dsvdc_property_t* properties);
  void setcontrol(dsvdc_t* handle, const char* dsuid, int32_t value, int32_t group, int32_t zone);
  void setChannelValue(dsvdc_t* handle, const char* dsuid, bool apply, double value, int32_t channel);

public:
  void registerVDC(boost::shared_ptr<IVDC> vdsm);
  virtual boost::shared_ptr<IVDC> getVDC() const;

  void registerVDSD(boost::shared_ptr<IVDSD> vdc);

  virtual dsvdc_t* getHandle() const;

  void run();
  void shutdown();

private:
  void resetConnectionState();

  void processAnnouncement();

private:
  dsvdc_t* m_handle;
  bool m_ready;

  typedef  std::map<dsuid_t, boost::shared_ptr<IVDSD> > VDSD_COLLECTION;
  typedef  std::map<dsuid_t, boost::shared_ptr<IVDSD> >::iterator VDSD_ITERATOR;
  typedef  std::map<dsuid_t, boost::shared_ptr<IVDSD> >::const_iterator VDSD_CONST_ITERATOR;

  VDSD_COLLECTION m_vdsd;
  Mutex m_AccessMutex;
  boost::shared_ptr<IVDC> m_vdc;
};

#endif // VDCAccess_H
