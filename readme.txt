How to generate python wrapper:
swig -c++ -python VDCPYLIB.i

// how to build:
qmake
make
cp lib_VDCLib.so _VDCLib.so 

// how to use:

import VDCLib

vdc1 = VDCLib.VDCPYLib()
vdc1.setVdcName("myTest")
vdc1.setVdcDsuid("84855887E07459ACC0A706EC2CACEBA000")
vdc1.startup()

sensor = vdc1.createDeviceSensor("84855887E07459ACC0A706EC2CACEBA010")
sensor.setData(23)




